# Lab 02: Lab 01 Analysis

[Click here](../../README.md) to go back to README.


The following document will describe in details about our **requirement gathering** and **modelling** process for building a University Module Registration System.

### Table of Contents

| **No.** | **Section**                           |
|---------|---------------------------------------|
| 1       | [Textual analysis](#textual-analysis) |
| 2       | [Use-case Diagram](#use-case-diagram) |
| 3       | [User Personas](#user-personas)       |

## Textual Analysis
The text below was taken from *The Brief* provided in the Lab-01 Sheet for Week-01.

Analysed Text:
![Lab 01 Textual Analysis](images/textual-analysis.png "Textual Analysis")

Color-coding was used to identify the various elements given in the brief.
Below are the descriptions of each color and their groupings.
- **Green:** Actors (**light:** Student/**medium:** Staff/**dark:** Systems)
- **Orange:** Use case (mainly involving students)
- **Purple:** Use case (mainly involving student services)
- **Blue:** Additional requirement (for student choosing modules with other prerequisites)

Below is the extracted texts, including their identified types and short descriptions.

Extracted Text:
![Lab 01 Extracted Text](images/textual-analysis-extracted.png "Extracted Text")

The actors identified were:
- **Students**
  + This actor are the **main target** of the system..
  + There are several archetype for this actor, summarized in the **personas** section below.
  + Should be able to ***register***, ***choose modules***, ***submit***, and ***see their enrolment status***.
- **Student Services** (Staff)
  + This actor mainly replies to *Students* actor.
  + They are responsible for ***receiving inputs*** from *Students* and ***validating the inputs***.
  + Able to ***approve enrolment*** by **default**
  + They are able to ***access most records***, ***generate reports***.
- **Module Convener** (Staff)
  + A variant of Staff.
  + Should be able to access ***module information*** and ***records of students enrolling in their module***.
  + Should be able to ***generate timetable*** for their module.
  + Should be able to ***approve enrolment*** if *all prerequisites fulfilled*.
- **Head of Teaching** (Staff)
  + A variant of Staff.
  + Should be able to ***approve enrolment*** if *all prerequisites fulfilled*.
  + They are able to ***access most records***, ***generate reports***.
- **BlueCastle** (System)
  + An existing system
  + Used to ***store students records*** and ***generate reports***.
  + Accessible by staff
- **Timetabling** (System)
  + An exiting system
  + Used to ***generate timetable*** and ***class size***.
  + Accessible by staff


## Use-Case Diagram

![Lab 01 Use-Case Diagram](images/use-case-diagram.png "Use-Case Diagram")

**This is the description for the use case diagram above.**

The whole system are divided into mainly **two parts**: **the main system** and **the admin system**.
The main system's main function is about interaction with four main users: **students, module conveners, the head of teaching and student service**. And the admin system is responsible for providing information of students to module conveners and the head of teaching. Besides, it is also in charge of interacting with other official software such as **Timetabling** and **BlueCastle**.

The main users, can be divided into two kinds. They are on two sides of main system respectively. The students ought to be able to sign in, collect and submit form distributed from student service. If the form is in 60-60 style(every semester 60 credits), then the main system should approve *by default*. Otherwise, the head of teaching and corresponding module convener should approve further. In the diagram two possible situation has been given. Notably, there maybe other circumstance that further approval is needed.  


## User Personas
Click on the persona names below to see their profile details.
##### Students
- [Jake Gonzales](personas/student-01_Jake.md)
- [Michelle Wu](personas/student-02_Michelle.md)
- [Chloe Everett-Yang](personas/student-03_Chloe.md)

These three personas for students were made to reflect most if not all of the possible types of students in the UNNC campus.

##### Teaching Staff
- [Amy Lin](personas/staff-01_Amy.md) - Student services
- [Phil Norton](personas/staff-02_Phil.md) - Module Convener
- [Alex Williams](personas/staff-03_Alex.md) - Head of Teaching

These three personas for staff were made to represent each variant of staff as shown above.
