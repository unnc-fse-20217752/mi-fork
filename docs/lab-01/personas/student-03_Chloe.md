# User Persona 03 - Student

#### Chloe Everett-Yang | The Postgraduate Student
**PG - Teaching English to Speakers of Other Languages (TESOL)**\
![Chloe's Photo](img/student03-chloe.jpg)
> Okay now, let's begin our lesson!

##### The facts
- Female
- 29 years-old
- Married, 1 child
- B.A. English Language and Applied Linguistics
- Speaks English and Indonesian proficiently, knows a bit of French and Mandarin
- Lives off-campus with her husband and child
- Above average computer skill
  - Able to organize online classes for her students
  - Heavy user of word processing and spreadsheet software
  - Knows how to write academic and professional papers digitally
- Moderate user of internet (~ 4 hours per day)
- Tends to work on laptop, yet very reachable on mobile
  - Does not understand technical computer stuff
  - Relies on IT services to help her with computer problems
- Can be usually seen typing on her laptop while enjoying a caffé latte in coffee shops


##### Description

Chloe is an English teacher who is currently continuing her studies at the Masters level in TESOL.
She is a dedicated teacher and have a passion for teaching the English language. She is kind-hearted and is able to work well with younger students. She has been teaching for the past 6 years prior to continuing her postgraduate studies.

After getting married to her husband - also a teacher - she took his last name and they both moved to China to teach in school. She teaches First Language English in the Cambridge's International General Certificate for Secondary Schooling (Cambridge IGCSE) curriculum. She took a sponsorship offer from her school to continue her studies in postgraduate level.

##### Key Attributes
- Passionate
- Focused
- Natural leader
- Well-informed
- Always strives to improve
- A bit short-tempered
