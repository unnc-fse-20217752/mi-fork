# User Persona 01 - Student

#### Jake Gonzales | The International Student
**UG Year 3 - Computer Science Major**\
![Jake's Photo](img/student01-jake.jpg)
> Coursework deadline tomorrow, I'm gonna pull an all-nighter!

##### The facts
- Male
- 21 years-old
- Currently single, previously had a significant other
- Highschool graduate
- Speaks English at native level, knows Spanish and a little bit of Mandarin Chinese
- Proficient in using computer, able to do programming work
- Extensive user of internet (> 6 hours per day)
- Usually accesses the web through his laptop
  - Uses ethernet cable in the dorm
  - Uses wireless connection when in campus teaching areas
  - Has his mobile phone handy when the school's internet is slow or unavailable
  - Sometimes uses his phone to quickly browse things
- Often times sleeps really late at night (after 12 AM)
- Usually submits his work and assignment close to the deadline
- Loves to drink coffee while doing his assignments

##### Description

Jake is a full time student in UNCC. He came from the United States and is one of the many
international students on campus. He really loves his computer, and that's also why he
decided to study B.Sc. Computer Science in UNNC. Although in terms of grades he is above
the average, he has issues in time-management and often submits his assignments really
close to the deadline.

He's proficient in anything technology. He can navigate easily through most systems and
can figure out how to use a system or software after little bit of tinkering with it. He
doesn't have any problem using most university registration system. He always have either
his laptop or mobile phone with him.

##### Key Attributes
- Fast-learner
- Disorganized
- Loses focus easily
- Brilliant
- Socially-inept at the beginning, but able to follow after
- Types at godly speed
