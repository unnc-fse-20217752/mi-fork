# User Persona 03 - Joint-Account User

#### John Warwick | The International Student
**UG Year 3 - Computer Science Major** | shares bank account with his parents

![John's Photo](img/actor03-john.jpg)
> I'm studying hard to get and keep that scholarship!

##### The facts
- Male
- 21 years-old
- Single
- Highschool graduate
- Speaks English at native level, knows Spanish and a little bit of Mandarin Chinese
- Still use spreadsheets to manage his tight budgets
- Good at math and logic
- Extensive user of internet (> 6 hours per day)
- Usually accesses the web through his laptop
  - Uses ethernet cable in the dorm
  - Uses wireless connection when in campus teaching areas
  - Able to use almost any software.
- Often times sleeps really late at night (after 12 AM)
- Loves to drink coffee while doing his assignments
- Do not waste money and time
- Sometimes he goes out to drink with his friends

##### Description

Jake is a full time student in UNCC. He came from the United States and is one of the many international students on campus. He has been a customer of Bank of China since his first year in China. His Chinese language level is comparatively low, thus he often has problem when using Chinese-based apps. His parents also have access to his bank account for them to monitor his spending.

His bank account receives income from multiple sources - his parents, scholarship and part-time jobs as a tutor. He'd like an app that is able to handle categorising his various income so that he won't mix them up, since different income source is used for different things. Currently, he's still using spreadsheets to manage his money.

##### Key Attributes
- Fast-learner
- Disorganized
- Loses focus easily
- Loves to save money
- Have multiple source of income for his account
