import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.lang.Boolean.*;

/** [Sub-System] Transaction data management **/
public class BoCTransaction {
    private String transactionName;
    private BigDecimal transactionValue;
    private int transactionCategory;
    private Date transactionTime;

    /*
    private int namePointer, valuePointer; 	// variables added for isComplete() method
    */                                      // to check if transaction exists

    private boolean nameExists, valueExists;

    /** Creates a new transaction record using default values. **/
    public BoCTransaction() {
        transactionName 	= null;
        transactionValue 	= null;
        transactionCategory = 0;
        transactionTime 	= null;

        /*
        namePointer 		= 1;
        valuePointer		= 1;
        */

        nameExists = false;
        valueExists = false;

    }

    /**
     * Creates a new transaction record.
     * @param tName 	The name of the transaction of type String - will be set to transactionName.
     * @param tValue 	The value of the transaction of type BigDecimal - will be set to transactionValue.
     * @param tCat 		The category of type int that represents a transaction category -w ill be set to transactionCategory.
     */
    public BoCTransaction(String tName, BigDecimal tValue, int tCat) {
        transactionName = tName;
        transactionValue = tValue;
        transactionCategory = tCat;
        transactionTime = new Date();

        /*
        namePointer 		= 1;
        valuePointer		= 1;
        */

        nameExists = false;
        valueExists = false;
    }

    /**
     * Getter for transaction name
     *
     * @return name of transaction
     */
    public String transactionName() {
        return transactionName;
    }

    /**
     * Getter for value of transaction
     *
     * @return value of transaction
     */
    public BigDecimal transactionValue() {
        return transactionValue;
    }

    /**
     * Getter for transaction category
     *
     * @return transaction category (integer that represents the category index)
     */
    public int transactionCategory() {
        return transactionCategory;
    }

    /**
     * Getter for the time of when the transaction is made
     *
     * @return time when transaction created
     */
    public Date transactionTime() {
        return transactionTime;
    }

    /**
     * Sets new name for current transaction name.
     *
     * @param tName         The name of the transaction of type String - will be set to transactionName.
     * @throws Exception    if transaction name exists or no parameter inputted.
     */
    public void setTransactionName(String tName) throws Exception {
        if (tName != null && !nameExists) {
            transactionName = tName;
            nameExists = true;          // Sets the name of category to exist, prevent using same name for other category.
        } else if (nameExists) {
            throw new Exception ("Name exists");
        } else {
            throw new Exception ("No parameter input");
        }
    }

    /**
     * Sets new value for current transaction.
     *
     * @param tValue    The value of the transaction of type BigDecimal - sets to transactionValue.
     */
    public void setTransactionValue(BigDecimal tValue) throws Exception {
        if (tValue.compareTo(new BigDecimal("0.00")) == 1 && !valueExists) {
            // 1 means bigger, -1 means smaller, 0 means same
            transactionValue = tValue;
            valueExists = true;
        } else if (tValue.compareTo(new BigDecimal("0.00")) <= 0){
            throw new Exception ("Transaction value cannot be lower");
        } else if (valueExists) {
            throw new Exception ("Value not available");
        } else {
            throw new Exception ("No parameter input");
        }
    }

    /**
     * Sets the category for current transaction.
     *
     * @param tCat      The index number of the category to set - sets to transactionCategory
     */
    public void setTransactionCategory(int tCat) {
        if (tCat > 0) {
            transactionCategory = tCat;
        }
    }

    /**
     * Sets the creation time of a transaction
     *
     * @param tTime     The creation time of the transaction - sets to transactionTime
     */
    public void setTransactionTime(Date tTime) {
        if (tTime != null) {
            transactionTime = tTime;
        }
    }

    /**
     * Checks whether a transaction name already exists
     * and whether a value can be set for that transaction.
     * Author: Xiaosong Hu | Edited: Joseph Thenara
     */
    public String isComplete(){
        String status = "";
        if (transactionName!=null) {
            status += "Name: "+transactionName+"\n";
        }  //get the name of transaction

        if (transactionValue!=null) {
            status += "Value: "+transactionValue+"\n";
        }  //get the value of transaction

        if (!nameExists) {
            status += "Name can be set, ";
        } else {
            status += "Name cannot be set, ";
        }  //check if the name can be set

        if(!valueExists){
            status += "value can be set.";
        } else{
            status += "value cannot be set.";
        }  //check if the value can be set

        return status;
    }

    /**
     * Overrides normal return.
     * When this class is called as a String, this function
     * will formats the return value to clearly display
     * the transaction date, name, and value.
     *
     * @return  Transaction record in the format of "dd-MM-yyyy HH:mm:ss | Name - ¥XXX.XX"
     *
     * Edited by Joseph to include time.
     */
    @Override
    public String toString() {
        // return transactionName + " - ¥" + transactionValue.toString();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String dateString = dateFormat.format(transactionTime);

        String toStringResult = dateString + " | " + transactionName + " - ¥" + transactionValue.toString();

        return toStringResult;

    }

}
