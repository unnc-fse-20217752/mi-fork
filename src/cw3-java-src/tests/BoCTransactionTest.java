import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class BoCTransactionTest {


    static String strExpect, strActual, name1, name2;
    static int intExpect, intActual, intExpect2, zero;
    static BigDecimal bigExpect,bigExpect2, bigActual, bigZero;
    static Date currentDate, newDate;

    static BoCTransaction txn;

    @BeforeAll
    static void setup() {       // Variable initialisations
        strExpect   = "Phone Bill";
        strActual   = "";
        name1       = "testName1";
        name2       = "altName2";
        intExpect   = 1;
        intExpect2  = 10;
        intActual   = zero = 0;
        bigExpect   = BigDecimal.valueOf(37.99);
        bigExpect2  = BigDecimal.valueOf(38.99);
        bigActual   = bigZero = BigDecimal.valueOf(0);

        txn = new BoCTransaction(strExpect, bigExpect, intExpect);
        currentDate = new Date();
    }

/* Author: Moeka | STARTS */
    @Test
    void testTransactionName() {        // This test is to test if transactionName method can return transactionName as a string
        txn = new BoCTransaction(strExpect, bigExpect, intExpect);
        strActual = txn.transactionName();      // get transactionName by transactionName method
        assertEquals(strExpect, strActual);     // compare with the desired value
    }

    @Test
    void testTransactionValue () {      // This test is to test if transactionValue method can return transactionValue as a BigDecimal
        txn = new BoCTransaction(strExpect, bigExpect, intExpect);
        bigActual = txn.transactionValue();     // get transactionValue by transactionValue method
        assertEquals(bigExpect, bigActual);     // compare
    }

    @Test
    void testTransactionCategory () {   // This test is to test if transactionCategory method can return transactionCategory as an int
        intActual = txn.transactionCategory();  // get transactionCategory by transactionCatefory method
        assertEquals(intExpect, intActual);     // compare
    }

    @Test
    void testTransactionTime () {       // This test is to test if transactionTime method can return transactionTime as a Date object
        strActual = txn.transactionTime().toString();   // get transactionTime by transactionTime method
        strExpect = currentDate.toString();
        assertEquals(strExpect, strActual);             // Compare
    }
/* Author: Moeka | ENDS */

/* Author: Xuan Huang | STARTS */
    @Test
    void testSetTransactionName1() {    // test setTransactionName method with null value, it will not change anything.
        try {
            txn.setTransactionName(null);
        } catch (Exception e) {
            if (e.getClass() == Exception.class){
                return;
            } else {
                fail("Wrong exception");
            }
        }

        assertNotNull(txn.transactionName());
        assertEquals("Phone Bill", txn.transactionName());
    }

    @Test
    void testSetTransactionName2() throws Exception {   // test setTransactionName method with normal value, it will pass as expected
        txn = new BoCTransaction(strExpect, bigExpect, intExpect);
        txn.setTransactionName(name1);
        strActual = txn.transactionName();
        assertEquals(name1, strActual);
    }
/* Author: Xuan Huang | ENDS */

/* Author: Xiaosong Hu | STARTS */
    @Ignore
    // Test has been modified to fit with new changes on the class (see the test below).
    void testSetTransactionName3() throws Exception {
        txn.setTransactionName(name1);
        txn.setTransactionName(name2);
        strActual = txn.transactionName();
        assertEquals(name1, strActual);
    }

    @Test // Test modified by : Joseph Thenara
    void testSetTransactionName3new() throws Exception {   // test setTransactionName method twice, it should be name1 because it can only be set once as required in description file. But source code doesn't realize it
        txn    = new BoCTransaction(strExpect, bigExpect, intExpect);
        String stnExcExpected   = "java.lang.Exception: Name exists";

        txn.setTransactionName(name1);
        assertEquals(name1, txn.transactionName());

        try {
            txn.setTransactionName(name2);
        } catch (Exception e) {
            assertEquals(stnExcExpected, e.toString());
        }

    }

    @Ignore
    void testSetTransactionValue0() throws Exception {      // Test setTransactionValue method with 0.00
        // Test has been modified to fit with new changes on the class (see the test below).
        txn.setTransactionValue(bigZero);
        bigActual = txn.transactionValue();
        assertEquals(bigExpect, bigActual);
    }

    @Test   // Modified by: Joseph Thenara
    void testSetTransactionValue0new() {
        txn = new BoCTransaction(strExpect, bigExpect, intExpect);
        String excExpected = "java.lang.Exception: Transaction value cannot be lower";

        try {
            txn.setTransactionValue(bigZero);
        } catch (Exception e) {
            assertEquals(excExpected, e.toString());
        }

        bigActual = txn.transactionValue();
        assertEquals(bigExpect, bigActual);
    }

    @Test
    void testSetTransactionValue1() throws Exception {       // Test setTransactionValue method with normal value
        txn.setTransactionValue(bigExpect);
        bigActual = txn.transactionValue();
        assertEquals(bigExpect, bigActual);
    }

    @Ignore
    void testSetTransactionValue2() throws Exception {       // test setTransactionValue method twice, it should be bigExpect because it can only be set once as required in description file. But source code doesn't realize it
        // Test has been modified to fit with new changes on the class (see the test below).
        txn.setTransactionValue(bigExpect);
        txn.setTransactionValue(bigExpect2);
        assertEquals(bigExpect, txn.transactionValue());
    }

    @Test // Modified by : Joseph Thenara
    void testSetTransactionValue2new() throws Exception {
        txn    = new BoCTransaction(strExpect, bigExpect, intExpect);
        String stnExcExpected   = "java.lang.Exception: Value not available";

        txn.setTransactionValue(bigExpect2);
        assertEquals(bigExpect2, txn.transactionValue());

        try {
            txn.setTransactionValue(bigExpect2);
        } catch (Exception e) {
            assertEquals(stnExcExpected, e.toString());
        }
    }
/* Author: Xiaosong Hu | ENDS */

/* Author: Xuan Huang | STARTS */
    @Test
    void testSetTransactionCategory() {     // test setTransactionCategory method with several different values
        assertEquals(intExpect, txn.transactionCategory());
        txn.setTransactionCategory(intExpect2);
        intActual = txn.transactionCategory();
        assertEquals(intExpect2, intActual);
        txn.setTransactionCategory(zero);
        intActual = txn.transactionCategory();
        assertEquals(intExpect2, intActual);
        assertNotEquals(zero, intActual);
    }
/* Author: Xuan Huang | ENDS */

/* Author: Moeka | STARTS */
    @SuppressWarnings("deprecation")
    @Test
    void testSetTransactionTime () {        // compare original date with given date, and run setTransactionTime method with second date, compare it
        assertEquals(txn.transactionTime().toString(), currentDate.toString());
        newDate = new Date(2000, 11, 21, 21,46,11);
        txn.setTransactionTime(newDate);
        assertSame(txn.transactionTime(), newDate);
    }

    @Test
    void testSetTimeWithNull () {           // Test setTransactionTime Method with null value
        assertEquals(currentDate.toString(), txn.transactionTime().toString());
        txn.setTransactionTime(null);
        assertNotNull(txn.transactionTime());
    }

    @Ignore
    public void testToString () {           // test toString method
        // Test has been modified because it did not test the toString() function from BoCTransaction Class.
        // This test uses toString() method from java.math.BigDecimal.
        String actual = txn.transactionName() + " - ¥" + txn.transactionValue().toString();
        String expect = "Phone Bill - ¥37.99";
        assertEquals(expect, actual);
        System.out.println(expect);
        System.out.println(actual);
    }

    @Test   // Modified by: Joseph Thenara
    public void testToStringnew () {
        String actual, expected;
        String tsnName      = "Internet Bills";
        BigDecimal tsnBig   = BigDecimal.valueOf(119.99);
        int tsnInt          = 3;

        BoCTransaction tsn = new BoCTransaction(tsnName, tsnBig, tsnInt);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String dateString = dateFormat.format(tsn.transactionTime());

        expected = dateString + " | " + tsn.transactionName() + " - ¥" + tsn.transactionValue().toString();
        actual = tsn.toString();

        assertEquals(expected, actual);

    }

/* Author: Moeka | ENDS */

/* Author: Joseph Thenara | STARTS */
    @Test
    void testIsComplete() throws Exception {
        String newLine  = "\n", strResExpect;
        strResExpect    = "Name: " + strExpect + newLine +
                          "Value: " + bigExpect + newLine +
                          "Name can be set, value can be set.";

        // System.out.println(txn.isComplete());
        assertEquals(strResExpect, txn.isComplete());

        txn.setTransactionName("Mobile Phone Bill");
        // System.out.println(txn.isComplete());

        strResExpect    = "Name: Mobile Phone Bill" + newLine +
                          "Value: " + bigExpect + newLine +
                          "Name cannot be set, value can be set.";

        assertEquals(strResExpect, txn.isComplete());

        txn.setTransactionValue(new BigDecimal("332.00"));
        // System.out.println(txn.isComplete());

        strResExpect    = "Name: Mobile Phone Bill" + newLine +
                          "Value: 332.00" + newLine +
                          "Name cannot be set, value cannot be set.";

        assertEquals(strResExpect, txn.isComplete());
    }

}
/* Author: Joseph Thenara | ENDS */