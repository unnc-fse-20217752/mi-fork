# Mission Impossible: FSE

Welcome to the **Mission Impossible readme** - the __first place__ people look at for all info!

## Table of Contents
| **No.** | **Section**                       | **Quick Navigation**                        |
|---------|-----------------------------------|---------------------------------------------|
| 1       | Contributors                      | [Get to know us!](#the-mi-team)             |
| 2       | Lab analyses and tasks            | [Check our work!](#analyses-from-labs)      |
| 3       | Assessment Deliverables           | [Assessment info](#assessment-deliverables) |
| 4       | Source Code                       | [Inspect code](#source-code)                |


## The MI Team
To get to know us, click the names to view our profile.
##### Members
Listed as per the Team List order.

- [Xuan Huang](contributors/xuanhuang.md)
- [Xiaosong Hu](contributors/Xiaosong.md)
- [Jialin Li (**pending**)](contributors/[placeholder])
- [Qianjun Ma](contributors/Qianjun.md)
- [Joseph Manuel Thenara](contributors/josephthenara.md)
- [Moeka Amatsuji](contributors/Moeka.md)


##### Module Convener
- [Lee Boon Giin (Bryan)](contributors/bryanlbg.md)


## Analyses from Labs
<table>
  <tr>
    <td align="center"><b>Lab No.</b></td>
    <td align="center"><b>Description</b></td>
    <td align="center"><b>Links</b></td>
  </tr>
  <tr align="center">
    <th colspan="3">Documentation</th>
  </tr>
  <tr>
    <td>Lab 01</td>
    <td>Introduction to using Visual Paradigm</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td>Lab 02</td>
    <td>Documentation of Lab 01</td>
    <td><a href="docs/lab-01/lab1analysis.md">Documentation</a></td>
  </tr>
  <tr>
    <td>Lab 03</td>
    <td>Report, including Activity Diagram and Sequence Diagram</td>
    <td><a href="docs/lab-03/lab03report.md">Report</a></td>
  </tr>
  <tr>
    <td>Lab 04</td>
    <td>CW 1 - Requirements (Assessment)</td>
    <td><a href="docs/lab-04/lab04report.md">Report</a></td>
  </tr>
  <tr>
    <td>Lab 05</td>
    <td>CW 2 - Specifications (Assessment)</td>
    <td><a href="docs/lab-05/lab05report.md">Report</a></td>
  </tr>
  <tr>
    <td>Lab 07</td>
    <td>Coursework 3 - JUnit Testing (Assessment)</td>
    <td><a href="docs/lab-07-cw3/cw3-report.md">Report</td>
</table>


## Assessment Deliverables
+ **Coursework 1 - Requirements:** [Report](docs/lab-04/lab04report.md)
+ **Coursework 2 - Specifications:** [Report](docs/lab-05/lab05report.md)
+ **Coursework 3 - JUnit Testing:** [Report](docs/lab-07-cw3/cw3-report.md) | [Test Plan](docs/lab-07-cw3/testplan.md) | [Source Code Directory](src/cw3-java-src)

## Source Code
<table>
  <tr>
    <td align="center"><b>Lab No.</b></td>
    <td align="center"><b>Description</b></td>
    <td align="center"><b>Links</b></td>
  </tr>
  <tr align="center">
    <th colspan="3">Java/JUnit Testing</th>
  </tr>
  <tr>
    <td>Lab 06</td>
    <td>JUnit Testing - MathModule (Individual)</td>
    <td>
      <a href="src/Lab06/placeholder">01 - Xuan Huang</a><br>
      <a href="src/Lab06/02-Xiaosong_Hu">02 - Xiaosong Hu</a><br>
      <a href="src/Lab06/placeholder">03 - Jialin Li</a><br>
      <a href="src/Lab06/placeholder">04 - Qianjun Ma</a><br>
      <a href="src/Lab06/05-Joseph_Thenara">05 - Joseph Thenara</a><br>
      <a href="src/Lab06/06-Moeka_Amatsuji">06 - Moeka Amatsuji</a><br>
    </td>
  </tr>
  <tr>
    <td>Lab 07</td>
    <td>Coursework 3 - JUnit Testing</td>
    <td>
      <a href="src/cw3-java-src">App Java Source Code</a>
    </td>
</table>
